import com.sun.jdi.Value;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Db extends Component {

    final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    final String URL = "jdbc:derby:parameterDB;create=true";
    final String USERNAME = "";
    final String PASSWORD = "";

    //Létrehozzuk a kapcsolatot (hidat)
    Connection conn = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;

    //Megpróbáljuk életre kelteni
    public Db() {

        try {
            conn = DriverManager.getConnection(URL);
            System.setProperty("derby.language.sequence.preallocator", "1");
            System.out.println("A híd létrejött");
        } catch (SQLException ex) {
            System.out.println("Valami baj van a connection (híd) létrehozásakor.");
            System.out.println("" + ex);
        }

        //Ha életre kelt, csinálunk egy megpakolható teherautót
        if (conn != null) {
            try {
                createStatement = conn.createStatement();
            } catch (SQLException ex) {
                System.out.println("Valami baj van van a createStatament (teherautó) létrehozásakor.");
                System.out.println("" + ex);
            }
        }

        //Megnézzük, hogy üres-e az adatbázis? Megnézzük, létezik-e az adott adattábla.
        try {
            dbmd = conn.getMetaData();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a DatabaseMetaData (adatbázis leírása) létrehozásakor..");
            System.out.println("" + ex);
        }

        try {
            ResultSet rs = dbmd.getTables(null, "APP", "USERS", null);
            if (!rs.next()) {
               /*  createStatement.execute("drop table foglal");
                createStatement.execute("drop table terem");
                createStatement.execute("drop table tanacs");*/


               //createStatement.execute("ALTER TABLE terem ALTER COLUMN ugyszak set DATA TYPE VARCHAR(10)");

            createStatement.execute("CREATE TABLE terem("+
                            "varos VARCHAR(25) NOT NULL," +
                            "teremSzam VARCHAR(25) NOT NULL," +
                            "ugyszak VARCHAR(10) NOT NULL," +
                            "PRIMARY KEY(teremSzam))");

             createStatement.execute("CREATE TABLE tanacs("+
                       "tanacsSzam VARCHAR(25) NOT NULL," +
                       "varosTanacs VARCHAR(25) NOT NULL,"+
                       "PRIMARY KEY(tanacsSzam))");


              createStatement.execute("CREATE TABLE foglal("+
                        "FoglalId INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY(Start with 1, Increment by 1),"+
                        "teremSzam VARCHAR(25) NOT NULL," +
                        "tanacsSzam VARCHAR(25) NOT NULL," +
                        "datum VARCHAR(25) NOT NULL," +
                        "kezdes VARCHAR(25)NOT NULL," +
                        "veg VARCHAR(25)NOT NULL," +
                        "ugyszak VARCHAR(25)NOT NULL,"+
                        "FOREIGN KEY(teremSzam) REFERENCES terem(teremSzam)," +
                        "FOREIGN KEY(tanacsSzam) REFERENCES tanacs(tanacsSzam))");
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van az adattáblák létrehozásakor.");
            System.out.println("" + ex);
        }
    }

    /* Select Qurey Kiírarása Jtext-be*/
    public String runQuerySelect(String query) {
        /*
        String res="";
        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(query);

            preparedStatement.execute();

           int i=1;
          for( ResultSet rs = createStatement.executeQuery(query); rs.next();){
                res= rs.getString(i);
                i++;
            }   */

        String result = "";
        Statement stmt = null;
        ResultSet rs = null;
        String newLine = System.getProperty("line.separator");
        try {

            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columns = rsmd.getColumnCount();
            while (rs.next()) {
                /* Columns numbers offset from one */
                for (int i = 1; i <= columns; i++) {
                    result += "   " + rs.getString(i);
                }
                result += newLine;
            }
            // outputArea.setText(result);
        } catch (SQLException sqe) {
            //do something
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException sqe) {
            }

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException sqe) {
            }


        }

        return result;

    }

    /* Insert Qurey */
    public String runQueryInsert(String query) {
        String result = "";
        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(query);

            preparedStatement.execute();

        } catch (SQLException sqle) {
            JOptionPane.showConfirmDialog(null, "Hiba történt a qurey futtatásakor: " + sqle);
        } catch (Exception e) {
            System.out.println(e);
        }
        result += "A változtatás megtörtént az adatbázisban";

        return result;
    }

    /* Terem hozzáadása */
    public void teremMent(String varos, String terem, String ugyszak) {
        try {
            String sql = "insert into terem values(?,?,?)";
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
            preparedStatement.setString(1, varos);
            preparedStatement.setString(2, terem);
            preparedStatement.setString(3, ugyszak);
            preparedStatement.execute();

        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a terem hozzáadásakor   " + ex);
        }
    }

    /*Tanács hozzáadása*/
    public void tanacsMent(String tanacs, String varosTanacs) {
        try {
            String sql = "insert into tanacs values(?,?)";
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
            preparedStatement.setString(1, tanacs);
            preparedStatement.setString(2, varosTanacs);
            preparedStatement.execute();


        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a tanacs hozzáadásakor   " + ex);
        }

    }

    /*Terem kiolvasása String [] array-ba*/
    public String[] terem(String varos) {
        String sql = "select teremSzam from terem where varos=?";
        String teremSzamString = "";

        ArrayList<String> list = new ArrayList();

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
            preparedStatement.setString(1, varos);

            for (ResultSet rs = preparedStatement.executeQuery(); rs.next(); ) {

                list.add(rs.getString("teremSzam"));
            }


        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a terem kiolvasásakor  " + ex);
        }

        String[] teremSzam = new String[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            teremSzam[i] = (String) list.get(i);
        }

        for (String x : teremSzam) {
            System.out.println(x);
        }

        return teremSzam;

    }


    //Tanacs visszadása String [] array-ba
    public String[] tanacs(String varos) {
        String sql = "select tanacsSzam from tanacs where varosTanacs=?";
        String tanacsSzamString = "";

        ArrayList<String> list = new ArrayList();

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
            preparedStatement.setString(1, varos);

            for (ResultSet rs = preparedStatement.executeQuery(); rs.next(); ) {

                list.add(rs.getString("tanacsSzam"));
            }


        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a terem kiolvasásakor  " + ex);
        }

        String[] tanacsSzam = new String[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            tanacsSzam[i] = (String) list.get(i);
        }

        for (String x : tanacsSzam) {
            System.out.println(x);
        }

        return tanacsSzam;

    }

    /*Fejléc visszaadássa Vectorban*/

    public Vector header() {

        String query = "Select * from foglal";

        Vector header = new Vector();


        Statement stmt = null;
        ResultSet rs = null;
        String newLine = System.getProperty("line.separator");
        try {

            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();


            int columns = rsmd.getColumnCount();
            /* Retrieve column names */
            for (int i = 1; i <= columns; i++) {
                header.add(rsmd.getColumnName(i));
            }

        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "A keresés gomb nem jó " + ex);
        }

        System.out.println(header);


        return header;

    }

    /*Terem foglaltság visszadása Vectorban */

   public Vector teremVector(String teremSzam, String datumString){
   String query = "Select foglalid,teremszam, tanacsszam,ugyszak, datum, kezdes, veg from foglal where teremSzam=" + "'" + teremSzam + "'" + " and datum=" + "'" + datumString + "'"+"ORDER BY kezdes asc ";
  /* String query1 ="Select FoglalId, teremSzam, tanacsSzam, datum, Cast(kezdes as time) as timeKezd, Cast(veg as time) as timeVeg, ugyszam from foglal" +
           "where teremSzam=" + "'" + teremSzam + "'" + " and datum=" + "'" + datumString + "'"+"ORDER BY timeKezd " ;*/

   Vector result = new Vector();

    Statement stmt = null;
    ResultSet rs = null;
    String newLine = System.getProperty("line.separator");
        try

    {

        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();


        int columns = rsmd.getColumnCount();

        while (rs.next()) {
            Vector row = new Vector();

            /* Columns numbers offset from one */
            for (int i = 1; i <= columns; i++) {
                row.add(rs.getObject(i));
            }
            result.add(row);
        }
    }catch(
    SQLException ex)

    {
        JOptionPane.showConfirmDialog(null, "A keresés gomb nem jó " + ex);
    }


        for(
    int i = 0; i<result.size();i++)

    {
        System.out.println(result.get(i));
    }

    return result;
   }

    /*Terem össz foglaltság visszadása Vectorban */

    public Vector teremVectorOssz(String varos, String datumString){
       /* String query = "Select * from foglal join terem on foglal.teremSzam=terem.teremSzam where terem.varos="
                + "'" + varos + "'" + " and foglal.datum=" + "'" + datumString + "'"+"ORDER BY terem, kezdes ";*/

        String query2 ="Select foglal.FoglalId, foglal.teremSzam, foglal.tanacsSzam, foglal.ugyszak,foglal.datum, foglal.kezdes, foglal.veg" +
                " from foglal, terem where foglal.teremSzam = terem.teremSzam and terem.varos="+"'"+varos+"'"+" and foglal.datum="+"'"+datumString+"'"+"order by foglal.kezdes";

      /*  String query1 = "Select * from foglal";*/

        Vector result = new Vector();

        Statement stmt = null;
        ResultSet rs = null;
        String newLine = System.getProperty("line.separator");
        try

        {

            stmt = conn.createStatement();
            rs = stmt.executeQuery(query2);
            ResultSetMetaData rsmd = rs.getMetaData();


            int columns = rsmd.getColumnCount();

            while (rs.next()) {
                Vector row = new Vector();

                /* Columns numbers offset from one */
                for (int i = 1; i <= columns; i++) {
                    row.add(rs.getObject(i));
                }
                result.add(row);
            }
        }catch(
                SQLException ex)

        {
            JOptionPane.showConfirmDialog(null, "A keresés gomb nem jó " + ex);
        }


        for(
                int i = 0; i<result.size();i++)

        {
            System.out.println(result.get(i));
        }

        return result;
    }

    /* Kezdés-is String [] visszaadása , Befejezési String [] visszaadása */

    public String [] kezdArray(String teremSzam, String datum){
        String sql = "select kezdes from foglal where teremSzam="+"'"+teremSzam+"'"+" and datum="+"'"+datum+"'";

        ArrayList<String> list = new ArrayList();

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);

            for (ResultSet rs = preparedStatement.executeQuery(); rs.next(); ) {

                list.add(rs.getString("kezdes"));
            }


        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a kezd kiolvasásakor  " + ex);
        }

        String[] kezd = new String[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            kezd[i] = (String) list.get(i);
        }

        for (String x : kezd) {
            System.out.println(x);
        }

        return kezd;


    }
    public String [] vegArray(String teremSzam, String datum){
        String sql = "select veg from foglal where teremSzam="+"'"+teremSzam+"'"+" and datum="+"'"+datum+"'";

        ArrayList<String> list = new ArrayList();

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);

            for (ResultSet rs = preparedStatement.executeQuery(); rs.next(); ) {

                list.add(rs.getString("veg"));
            }


        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a vég kiolvasásakor  " + ex);
        }

        String[] veg = new String[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            veg[i] = (String) list.get(i);
        }

        for (String x : veg) {
            System.out.println(x);
        }

        return veg;


    }

   /*Terem foglalása*/

    public void teremFoglal(String teremSzam,String tanacsSzam, String datum, String kezdes, String veg, String ugyszak){
        try {
            String sql = "insert into foglal(teremSzam, tanacsSzam, datum, kezdes, veg, ugyszak)values(?,?,?,?,?,?)";
            PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
            preparedStatement.setString(1,teremSzam);
            preparedStatement.setString(2,tanacsSzam);
            preparedStatement.setString(3,datum);
            preparedStatement.setString(4,kezdes);
            preparedStatement.setString(5,veg);
            preparedStatement.setString(6,ugyszak);

            preparedStatement.execute();

        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, "Valami baj van a terem hozzáadásakor   " + ex);
        }




    }


    /* ------------------------------------------------------- */

    /**File név ellenőrzése, hogy létezik-e már ilyen*/
    public boolean fileNameCheck(String fileNamePath){
        boolean gotName=false;
        try{
            Scanner scan = new Scanner(new File(fileNamePath+".pdf"));
            JOptionPane.showMessageDialog(null,"A file név már létezik. Próbálja újra!");
            gotName = true;

        }catch(FileNotFoundException e){System.out.println("Nincs ilyen");}

        return gotName;
    }
    public String fileChooser() {

        String fp="";
        try {
            JFileChooser open = new JFileChooser();
            open.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            int option = open.showOpenDialog(this);
            File filepath = new File(open.getSelectedFile().getPath());
            fp+=filepath;

        } catch (Exception e) {
            System.out.println("Nincs ilyen útvonal"+e);
        }

        return fp;
    }

    public void exportXls(JTable table, String fileNamePath){
        try
        {
            TableModel m = table.getModel();
            FileWriter fw = new FileWriter(fileNamePath+".xls", StandardCharsets.ISO_8859_1);

            for(int i = 0; i < m.getColumnCount(); i++){
                fw.write(m.getColumnName(i) + "\t");
            }
            fw.write("\n");
            for(int i=0; i < m.getRowCount(); i++) {
                for(int j=0; j < m.getColumnCount(); j++) {
                    fw.write(m.getValueAt(i,j).toString()+"\t");
                }
                fw.write("\n");
            }
            fw.close();
            Desktop.getDesktop().open(new File(fileNamePath + ".xls"));
        }
        catch(IOException e){ System.out.println(e); }
    }
}
