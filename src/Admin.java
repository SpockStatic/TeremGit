import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Admin extends JFrame {
    Db db = new Db();
/* Teremhez */
   private JLabel jLabelTerem;
   private JLabel jLabelVaros;
   private JLabel jLabelTeremSzam;
   private JLabel jLabelUgyszak;
   private JTextField jTextFieldVaros;
   private JTextField jTextFieldTeremszam;
   private JComboBox jComboBoxdugyszak;
   private JButton jButtonTeremMent;
   private JButton jButtonTeremTorol;
   private JLabel line01;

   private JLabel line02;

   private JLabel jLabelquery;

/* Tanácshoz */

   private JLabel jLabelTanacs;
   private JLabel jLabelTanacsSzam;
   private JTextField jTextFieldTanacszam;
   private JTextField jTextFieldTanacsVaros;
   private JButton jButtonTanacsMent;

   /* Sql query */
   private JTextArea jTextAreaQueryInsert;

   private JTextArea jTextAreaSelect;
   private JTextArea jTextAreaEredmeny;
   private JButton jButtonQuerySelect;
   private JButton jButtonQueryInsert;

   String [] ugy ={"C","B","Egyébb"};

   Admin(){
      super("Admin");
      setLayout(null);


      jLabelTerem = new JLabel("Terem hozzáadása");
      add(jLabelTerem);
      jLabelTerem.setBounds(10,10,150,25);

      jLabelVaros = new JLabel("Város: ");
      add(jLabelVaros);
      jLabelVaros.setBounds(10,50,75,25);

      jLabelTeremSzam = new JLabel("Terem: ");
      add(jLabelTeremSzam);
      jLabelTeremSzam.setBounds(10,100,75,25);

      jLabelUgyszak = new JLabel("Ügyszak: ");
      add(jLabelUgyszak);
      jLabelUgyszak.setBounds(10,150,75,25);

      jTextFieldVaros = new JTextField();
      add(jTextFieldVaros);
      jTextFieldVaros.setBounds(100,50,125,25);

      jTextFieldTeremszam = new JTextField();
      add(jTextFieldTeremszam);
      jTextFieldTeremszam.setBounds(100,100,125,25);

      jComboBoxdugyszak = new JComboBox(ugy);
      add(jComboBoxdugyszak);
      jComboBoxdugyszak.setBounds(100,150,125,25);

      jButtonTeremMent = new JButton("Ment");
      add(jButtonTeremMent);
      jButtonTeremMent.setBounds(10,200,100,25);

    /*  jButtonTeremTorol = new JButton("Töröl");
      add(jButtonTeremTorol);
      jButtonTeremTorol.setBounds(125,200,100,25);*/

      line01 = new JLabel("<html>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * </html>");
      add(line01);
      line01.setBounds(325,10,10,375);

      jLabelquery = new JLabel("<html> terem: <br/>   varos VARCHAR(25) <br/> teremSzam VARCHAR(25) <br/> ugyszak VARCHAR(10))" +
              "<br/>--------------------------------<br/>" +
              "tanacs: <br/> tanacsSzam VARCHAR(25) <br/> varosTanacs VARCHAR(25)" +
              "<br/>--------------------------------<br/>" +
              "foglal: <br/> FoglalId INT <br/> teremSzam VARCHAR(25) <br/> tanacsSzam VARCHAR(25) <br/> datum VARCHAR(25)" +
              "<br/> kezdes VARCHAR(25) <br/> veg VARCHAR(25) <br/>ugyszak VARCHAR(10) <br/> FOREIGN KEY(teremSzam) REFERENCES terem(teremSzam) <br/>FOREIGN KEY(tanacsSzam) REFERENCES tanacs(tanacsSzam)) </html>");

      add(jLabelquery);
      jLabelquery.setBounds(350,10,300,350);

      line02 = new JLabel("<html>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * </html>");
      add(line02);
      line02.setBounds(10,235,300,25);

      jLabelTanacs = new JLabel("Tanács hozzáadása");
      add(jLabelTanacs);
      jLabelTanacs.setBounds(10, 250, 150,25);

      jLabelTanacsSzam = new JLabel("Tanács: ");
      add(jLabelTanacsSzam);
      jLabelTanacsSzam.setBounds(10,280,75,25);

      jTextFieldTanacszam = new JTextField();
      add(jTextFieldTanacszam);
      jTextFieldTanacszam.setBounds(100,280,125,25);

      jLabelTanacsSzam = new JLabel("Város: ");
      add(jLabelTanacsSzam);
      jLabelTanacsSzam.setBounds(10,325,70,25);

      jTextFieldTanacsVaros = new JTextField();
      add(jTextFieldTanacsVaros);
      jTextFieldTanacsVaros.setBounds(100,325,125,25);

      jButtonTanacsMent  = new JButton("Ment");
      add(jButtonTanacsMent);
      jButtonTanacsMent.setBounds(10,360,100,25);

      line02 = new JLabel("<html>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * </html>");
      add(line02);
      line02.setBounds(10,380,700,25);

      jLabelquery = new JLabel("Select");
      add(jLabelquery);
      jLabelquery.setBounds(10,400,100,25);

      jLabelquery = new JLabel("Eredmény");
      add(jLabelquery);
      jLabelquery.setBounds(350,400,100,25);

      jLabelquery = new JLabel("Insert");
      add(jLabelquery);
      jLabelquery.setBounds(10,605,100,25);

      jTextAreaSelect = new JTextArea();
      JScrollPane scrollPaneQuery = new JScrollPane(jTextAreaSelect);
      scrollPaneQuery.setBounds(10,430,300,175);
      jTextAreaSelect.setFont(jTextAreaSelect.getFont().deriveFont(14f));
      add(scrollPaneQuery);

     jTextAreaEredmeny = new JTextArea();
      JScrollPane scrollPaneEredmeny = new JScrollPane(jTextAreaEredmeny);
      scrollPaneEredmeny.setBounds(350,430,300,410);
      jTextAreaEredmeny.setFont(jTextAreaEredmeny.getFont().deriveFont(14f));
      add(scrollPaneEredmeny);

      jButtonQuerySelect = new JButton("Futtat");
      add(jButtonQuerySelect);
      jButtonQuerySelect.setBounds(210,605,100,25);

      jTextAreaQueryInsert = new JTextArea();
      JScrollPane scrollPaneInsert = new JScrollPane(jTextAreaQueryInsert);
      scrollPaneInsert.setBounds(10,635,300,175);
      jTextAreaQueryInsert.setFont(jTextAreaQueryInsert.getFont().deriveFont(14f));
      add(scrollPaneInsert);

      jButtonQueryInsert = new JButton("Futtat");
      add(jButtonQueryInsert);
      jButtonQueryInsert.setBounds(210,815,100,25);


      jButtonQuerySelect.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            String query = jTextAreaSelect.getText();
            System.out.println(query);
          //  db.runQuery(query);
            jTextAreaEredmeny.setText(db.runQuerySelect(query));
         }
      });

        jButtonQueryInsert.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            String query = jTextAreaQueryInsert.getText();
           jTextAreaEredmeny.setText(db.runQueryInsert(query));
         }
      });

        jButtonTeremMent.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              String varos = jTextFieldVaros.getText();
              String terem = jTextFieldTeremszam.getText();
              String ugyszak = jComboBoxdugyszak.getSelectedItem().toString();

              db.teremMent(varos,terem,ugyszak);
           }
        });

        jButtonTanacsMent.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              String tanacs = jTextFieldTanacszam.getText();
              String varos = jTextFieldTanacsVaros.getText();
              db.tanacsMent(tanacs,varos);

           }
        });

   }

}
