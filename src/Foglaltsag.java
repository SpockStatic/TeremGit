import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

public class Foglaltsag extends JFrame {

    /*Terem nevek*/
    private JLabel terem01;
    private JLabel terem02;
    private JLabel terem03;
    private JLabel terem04;
    private JLabel terem05;
    private JLabel terem06;
    private JLabel terem07;
    private JLabel terem08;
    private JLabel terem09;
    private JLabel terem10;
    private JLabel terem11;
    private JLabel terem12;
    private JLabel terem13;
    private JLabel terem14;
    private JLabel terem15;
    private JLabel terem16;
    private JLabel terem17;
    private JLabel terem18;
    private JLabel terem19;
    private JLabel terem20;
    private JLabel terem21;
    private JLabel terem22;
    private JLabel terem23;
    private JLabel terem24;
    private JLabel terem25;
    private JLabel terem26;
    private JLabel terem27;
    private JLabel terem28;
    private JLabel terem29;
    private JLabel terem30;

    /*Óra*/
    private JLabel ora08;
    private JLabel ora09;
    private JLabel ora10;
    private JLabel ora11;
    private JLabel ora12;
    private JLabel ora13;
    private JLabel ora14;
    private JLabel ora15;
    private JLabel ora16;

    /*Perc*/

    private JLabel perc15;
    private JLabel perc30;
    private JLabel perc45;
    private JLabel perc59;


    Foglaltsag(){
        super("Terem foglaltság");
        setLayout(null);

        ora08 = new JLabel("8:00");
        add(ora08);
        ora08.setBounds(125,10,100,25);

        perc15 = new JLabel("00");
        add(perc15);
        perc15.setBounds(80,35,20,25);

        perc30 = new JLabel("15");
        add(perc30);
        perc30.setBounds(100,35,20,25);

        perc45 = new JLabel("30");
        add(perc45);
        perc45.setBounds(120,35,20,25);

        perc59 = new JLabel("45");
        add(perc59);
        perc59.setBounds(140,35,20,25);


        ora09 = new JLabel("9:00");
        add(ora09);
        ora09.setBounds(250,10,100,25);

        perc15 = new JLabel("10");
        add(perc15);
        perc15.setBounds(200,35,20,25);

        perc30 = new JLabel("20");
        add(perc30);
        perc30.setBounds(220,35,20,25);

        perc45 = new JLabel("30");
        add(perc45);
        perc45.setBounds(240,35,20,25);

        perc59 = new JLabel("40");
        add(perc59);
        perc59.setBounds(260,35,20,25);


        terem01=new JLabel("term01");
        add(terem01);
        terem01.setBounds(10,50,50,25);


    }



}
