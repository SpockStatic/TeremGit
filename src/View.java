import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.util.Date;
import java.util.Vector;

public class View extends JFrame {
 Db db =new Db();
 City city = new City();

    private JLabel jLabelForAll;
    private JComboBox jComboBoxCity;
    private JComboBox jComboBoxTeremSzam;
    private JButton jButtonSelectCity;
    private JButton jButtonKeres;


    private JTextField text;
    private JLabel jLabelVaros;
    private JLabel jLabelForAllTerem;
    private JButton buttonDate;
    private JLabel jLabelTeremFoglalo;
    private JLabel jLabelVarosFoglal;
    private JLabel jLabelVarosGet;
    private JLabel jLabelTeremFoglal;
    private JLabel jLabelTeremGet;
    private JLabel jLabelTanacsFoglal;
    private JLabel jLabelDatumFoglal;
    private JLabel jLabelDatumGet;
    private JLabel jLabelKezdes;
    private JLabel jLabelVeg;
    private JLabel jLabelOra;
    private JLabel jLabelUgyszak;
    private JComboBox jComboBoxTanacsFoglal;
    private JComboBox jComboBoxOraKezd;
    private JComboBox jComboBoxPercKezd;
    private JComboBox jComboBoxOraVeg;
    private JComboBox jComboBoxPercVeg;
    private JComboBox jComboBoxUgyszak;
    private JButton jButtonFoglal;
    private JButton jButtonOsszesKeres;
    private JButton jButtonVizualis;
    private JPasswordField jPasswordField;
    private JButton jButtonAdmin;

    View() {
     super("Terem");
     setLayout(null);

     jLabelForAllTerem = new JLabel("Terem keresés ");
        jLabelForAllTerem.setForeground(Color.red);
     add(jLabelForAllTerem);
        jLabelForAllTerem.setBounds(50,10,100,25);


     jLabelForAll = new JLabel("Város:" );
     add(jLabelForAll);
     jLabelForAll.setBounds(10,50,50,25);


    jComboBoxCity = new JComboBox(city.arrayCity);
    add(jComboBoxCity);
    jComboBoxCity.setBounds(100,50,110,25);

     jButtonSelectCity = new JButton("Kiválaszt");
     add(jButtonSelectCity);
     jButtonSelectCity.setBounds(100,85,110,25);

    jLabelVaros = new JLabel("Terem: ");
     add(jLabelVaros);
     jLabelVaros.setBounds(10,120,110,25);

     jComboBoxTeremSzam = new JComboBox();
     add(jComboBoxTeremSzam);
     jComboBoxTeremSzam.setBounds(100,120,110,25);

    buttonDate = new JButton("Dátum");
    add(buttonDate);
    buttonDate.setBounds(100,155,110,25);

    text  = new JTextField("év-hónap-nap");
    add(text);
    text.setBounds(100,195,110,25);

    jButtonKeres = new JButton("Keres");
    add(jButtonKeres);
    jButtonKeres.setBounds(10,230,200,25);

    jButtonOsszesKeres = new JButton("Összes Terem");
    add(jButtonOsszesKeres);
    jButtonOsszesKeres.setBounds(10,260,200,25);

    jButtonVizualis = new JButton("Vizális keresés");
    add(jButtonVizualis);
    jButtonVizualis.setBounds(10,290,200,25);



        /*Terem foglaló*/

        jLabelTeremFoglalo = new JLabel("Terem foglalás");
        add(jLabelTeremFoglalo);
        jLabelTeremFoglalo.setForeground(Color.red);
        jLabelTeremFoglalo.setBounds(300,10,100,25);

        jLabelVarosFoglal = new JLabel("Város: ");
        add(jLabelVarosFoglal);
        jLabelVarosFoglal.setBounds(300,50,100,25);

        jLabelVarosGet = new JLabel();
        add(jLabelVarosGet);
        jLabelVarosGet.setBounds(350,50,300,25);

        jLabelTeremFoglal = new JLabel("Terem: ");
        add(jLabelTeremFoglal);
        jLabelTeremFoglal.setBounds(300,85,100,25);

        jLabelTeremGet = new JLabel();
        add(jLabelTeremGet);
        jLabelTeremGet.setBounds(350,85,200,25);

        jLabelDatumFoglal = new JLabel("Dátum: ");
        add(jLabelDatumFoglal);
        jLabelDatumFoglal.setBounds(300,120,300,25);

        jLabelDatumGet = new JLabel();
        add(jLabelDatumGet);
        jLabelDatumGet.setBounds(350,120,200,25);

        jLabelTanacsFoglal = new JLabel("Tanács: ");
        add(jLabelTanacsFoglal);
        jLabelTanacsFoglal.setBounds(300,155,100,25);

        jComboBoxTanacsFoglal = new JComboBox();
        add(jComboBoxTanacsFoglal);
        jComboBoxTanacsFoglal.setBounds(400,155,100,25);

        jLabelKezdes = new JLabel("Kezdés ideje: ");
        add(jLabelKezdes);
        jLabelKezdes.setBounds(300,225,100,25);

        jLabelVeg = new JLabel("Befejezés ideje: ");
        add(jLabelVeg);
        jLabelVeg.setBounds(300,260,100,25);

        jLabelOra = new JLabel("    Óra           Perc");
        add(jLabelOra);
        jLabelOra.setBounds(400,200,100,25);

        jComboBoxOraKezd = new JComboBox(city.ora);
        add(jComboBoxOraKezd);
        jComboBoxOraKezd.setBounds(400,225,50,25);

        jComboBoxPercKezd = new JComboBox(city.perc);
        add(jComboBoxPercKezd);
        jComboBoxPercKezd.setBounds(460,225,50,25);

        jComboBoxOraVeg = new JComboBox(city.ora);
        add(jComboBoxOraVeg);
        jComboBoxOraVeg.setBounds(400,260,50,25);

        jComboBoxPercVeg = new JComboBox(city.perc);
        add(jComboBoxPercVeg);
        jComboBoxPercVeg.setBounds(460,260,50,25);

        jLabelUgyszak = new JLabel("Ügyszak: ");
        add(jLabelUgyszak);
        jLabelUgyszak.setBounds(300,295,100,25);

        jComboBoxUgyszak = new JComboBox(city.ugyszak);
        add(jComboBoxUgyszak);
        jComboBoxUgyszak.setBounds(400,295,50,25);

        jButtonFoglal = new JButton("Foglal");
        add(jButtonFoglal);
        jButtonFoglal.setBounds(300,330,210,25);


        /*Admin*/

        jPasswordField = new JPasswordField(20);
        add(jPasswordField);
        jPasswordField.setBounds(300,400,100,25);


        jButtonAdmin = new JButton("Admin");
        add(jButtonAdmin);
        jButtonAdmin.setBounds(410,400,100,25);


        jButtonSelectCity.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
      String varos = String.valueOf(jComboBoxCity.getSelectedItem());
      System.out.println(varos);
      jLabelVaros.setText(varos);

          DefaultComboBoxModel model1 = new DefaultComboBoxModel(db.terem(varos));
          DefaultComboBoxModel model2 = new DefaultComboBoxModel(db.tanacs(varos));

       jComboBoxTeremSzam.setModel(model1);
       jComboBoxTanacsFoglal.setModel(model2);


       }
     });

    buttonDate.addActionListener(new ActionListener() {
     @Override
     public void actionPerformed(ActionEvent e) {
      DatePicker dp = new DatePicker();
      dp.displayDate();
      text.setText(dp.setPickedDate());
     }
    });

    jButtonAdmin.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            String pass=jPasswordField.getText();
            if(pass.equals("zizi")) {

                Admin admin = new Admin();
                admin.setSize(700, 900);
                admin.setLocationRelativeTo(null);
                admin.setResizable(false);
                admin.setVisible(true);}
            else if(pass.length()<1){
                JOptionPane.showMessageDialog(null,"Jelszó megadása kötelező");
            }else JOptionPane.showMessageDialog(null,"Nem jó jelszó");

        }
    });

    jButtonKeres.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String teremSzam="";
            String datumString="";
            String varos="";
            try {
                teremSzam = jComboBoxTeremSzam.getSelectedItem().toString();
                datumString = text.getText();
                varos = jLabelVaros.getText();
            }catch (NullPointerException ex){
                JOptionPane.showMessageDialog(null,"Az összes mező kitőltése kötelező!");
            }
            if(varos.length()>0 && datumString.length()==10 && datumString.charAt(4)=='-' && datumString.charAt(7)=='-' && teremSzam.length()>0) {

                jLabelVarosGet.setText(varos);
                jLabelTeremGet.setText(teremSzam);
                jLabelDatumGet.setText(datumString);


                System.out.println(teremSzam + "  " + datumString);

              //  db.header();
                db.teremVector(teremSzam, datumString);

                Tablazat f = new Tablazat(varos, teremSzam, datumString);
                f.setSize(800, 400);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                f.show();

            }else JOptionPane.showMessageDialog(null,"Az összes mező kitőltése kötelező! Vagy a dátum formátuma nem megfelelő!");


        }
    });

    jButtonOsszesKeres.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String varos = jLabelVaros.getText();
            String datum = text.getText();

          //  db.header();
            db.teremVectorOssz(varos,datum);
            System.out.println(varos +" - - - - - "+datum);

            if(!varos.isEmpty() && !datum.isEmpty()) {

                Tablazat1 f = new Tablazat1(varos, "összes", datum);
                f.setSize(800, 400);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                f.show();
            }else JOptionPane.showMessageDialog(null,"A város és az időpont kitőltése kötelező!");

        }
    });

    jButtonVizualis.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
         /*   Foglaltsag f = new Foglaltsag();
            f.setSize(1500, 1000);
            f.setVisible(true);
            f.setResizable(false);
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); */
            String varos = jLabelVaros.getText();
            String datum = text.getText();

            if(!varos.isEmpty() && !datum.isEmpty() && !datum.equals("év-hónap-naop")  && datum.length()==10 && datum.charAt(4)=='-' && datum.charAt(7)=='-' && datum.length()>0) {

                FogPanel fp = new FogPanel(varos, datum);
                fp.setSize(1100, 800);
                fp.setVisible(true);
                fp.setResizable(false);
                //  fp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            }else JOptionPane.showMessageDialog(null,"A város és az időpont kitőltése kötelező!");

        }
    });

    jButtonFoglal.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            boolean mehet = false;
            boolean nagyonMehet = false;

            String terem = jLabelTeremGet.getText();
            String date = jLabelDatumGet.getText();
            String tanacs = String.valueOf(jComboBoxTanacsFoglal.getSelectedItem());
            String kezdes= String.valueOf(jComboBoxOraKezd.getSelectedItem())+":"+String.valueOf(jComboBoxPercKezd.getSelectedItem());
            String veg=String.valueOf(jComboBoxOraVeg.getSelectedItem())+":"+String.valueOf(jComboBoxPercVeg.getSelectedItem());
            String ugyszak = String.valueOf(jComboBoxUgyszak.getSelectedItem());

            System.out.println(terem+" "+tanacs+" "+date+" "+kezdes+" "+veg+" "+ugyszak);

            if(!terem.isEmpty() && !date.isEmpty() && !tanacs.isEmpty() && !date.equals("év-hónap-naop") && date.length()==10 && date.charAt(4)=='-' && date.charAt(7)=='-' && date.length()>0){
                nagyonMehet=true;
                System.out.println("nagyonMehet ture");
            }else JOptionPane.showMessageDialog(null, "Minden elem kitőltése kötelező!");

            String [] arrayVeg = db.vegArray(terem,date);
            String [] arrayKezd = db.kezdArray(terem,date);

            LocalTime [] VegtimeArray = new LocalTime[arrayVeg.length];
            LocalTime [] KezdTimeArray = new LocalTime[arrayKezd.length];

            LocalTime timeKezd = LocalTime.parse(kezdes);
            LocalTime timeVeg = LocalTime.parse(veg);


            if(VegtimeArray.length<1 && timeKezd.compareTo(timeVeg) <=0 ){
                mehet = true;
                System.out.println("mehet üres");
            }else
                for(int i=0; i<arrayVeg.length;i++){
                KezdTimeArray[i] = LocalTime.parse(arrayKezd[i]);
                VegtimeArray[i] = LocalTime.parse(arrayVeg[i]);

                System.out.println(VegtimeArray[i]+"   "+timeKezd+"   "+VegtimeArray[i].compareTo(timeKezd)+
                        "\n"+KezdTimeArray[i]+"  "+timeVeg+"   "+KezdTimeArray[i].compareTo(timeVeg)+"\n"+timeKezd +" :::: "+timeVeg);



                 if(VegtimeArray[i].compareTo(timeKezd) < 0 && timeKezd.compareTo(timeVeg) <0){
                    mehet = true;
                    System.out.println("mehet ture");
                }else mehet =false;

            }

            if(mehet == false){
                JOptionPane.showMessageDialog(null,"Az adott időpontban már van foglalás vagy\nA kezdés és vég időpont azonos!");
            }

            if (mehet && nagyonMehet) {
                db.teremFoglal(terem, tanacs, date, kezdes, veg, ugyszak);
                JOptionPane.showMessageDialog(null, "A "+terem+" lefoglalása\n"+kezdes+" - "+veg+" közötti időszakra\nmegtörtént.");
            }
        }
    });


    }

}
