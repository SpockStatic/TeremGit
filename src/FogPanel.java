import org.apache.derby.impl.store.raw.data.ContainerHandleActionOnCommit;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;


public class FogPanel extends JFrame {

    Db db = new Db();


    JLabel ora;
    JLabel jb;
    JButton [] terem = new JButton[20];


    JButton [] Button = new JButton[650];

    FogPanel(String varos, String datum){
        super(datum);


        String [] teremdb = db.terem(varos);
        int teremLength = teremdb.length;
        Arrays.sort(teremdb);


        JPanel p1 = new JPanel();
        //p1.setBounds(100,200,600,1000);
        //p1.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        p1.setLayout(new GridLayout(20,32));


        JPanel p2 = new JPanel();
       // p2.setBounds(10,10,300,150);
        p2.setLayout(new FlowLayout(FlowLayout.LEADING));
        p2.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));


        JPanel p3 = new JPanel();
       // p3.setBounds(20,10,200,800);
        p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
       // p3.setBorder(BorderFactory.createLineBorder(Color.BLACK,2));
        p3.setBorder(BorderFactory.createEmptyBorder(10,15,10,10));


        jb = new JLabel("Terem / Idő");
        p2.add(jb);
        //jb.setBounds(5,5,100,25);
        jb.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK,1),
                BorderFactory.createEmptyBorder(25,25,27,22)));


      /*  ora = new JButton("8 óra");
        p2.add(ora);
        ora.setEnabled(false);

      ora= new JButton("9 óra");
        p2.add(ora);
        ora.setBounds(210,5,100,25);
        ora.setMargin(new Insets(50,50,50,50));
        ora.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.CYAN,2),
                BorderFactory.createEmptyBorder(50,50,50,50)));*/




        /*terem01 = new JButton("terem");
        p3.add(terem01);
       // p2_1.add(terem01);
        terem01.setMargin(new Insets(10,10,10,10));*/



        //set JLabel properties such as border, text, font, etc.
        LineBorder line =  new LineBorder(Color.black, 1);
        Font TNR = new Font("Times New Roman", Font.BOLD, 12);
       /* JLabel black = new JLabel("Black", JLabel.CENTER);
        JLabel blue = new JLabel("Blue", JLabel.CENTER);
        JLabel cyan = new JLabel("Cyan", JLabel.CENTER);
        JLabel green = new JLabel("Green", JLabel.CENTER);
        JLabel magenta = new JLabel("Magenta", JLabel.CENTER);
        JLabel orange = new JLabel("Orange", JLabel.CENTER);
        JLabel num = new JLabel("num1",JLabel.CENTER);*/

        for(int i=8; i<16;i++){
            ora = new JLabel("<html>"+i+ " óra<br> 15p. 30p. 45p. 59p. </html>");
            ora.setFont(TNR);
            p2.add(ora);
            ora.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK,1),
                    BorderFactory.createEmptyBorder(10,10,10,10)));
            //ora.setEnabled(false);
        }


        for(int i=0;i<teremLength;i++){
            terem[i] = new JButton(teremdb[i]);
            terem[i].setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK,1),
                    BorderFactory.createEmptyBorder(7,10,9,20)));
            terem[i].setFont(TNR);
            p3.add(terem[i]);
        }



        for(int i=0;i<=639;i++){

            Button[i] = new JButton("id"+i);
            Button[i].setForeground(Color.GREEN);
            Button[i].setBackground(Color.GREEN);
            Button[i].setBorder(line);
            Button[i].setFont(TNR);
            Button[i].setToolTipText("  "+i);
            Button[i].setSize(25,25);
            p1.add(Button[i]);

        }



       /* for(int i=1;i<=32;i++){
            Button[i].setForeground(Color.RED);
            Button[i].setBackground(Color.RED);
            Button[i].setToolTipText("Hello amígó");
            Button[i].setText("fog");
        }*/

      /*  for(int i=33;i<=640;i++){
            Button[i].setForeground(Color.RED);
            Button[i].setBackground(Color.RED);
            Button[i].setToolTipText("Hello amígó");
            Button[i].setText("fog.. .");
        }*/



        // add set properties and add panel to JFrame
    /*    black.setOpaque(true);
        black.setForeground(Color.black);
        black.setBackground(Color.white);
        black.setBorder(line);
        black.setFont(TNR);
        black.setToolTipText("This is Black");
        add(black);

        // add set properties and add panel to JFrame
        blue.setOpaque(true);
        blue.setForeground(Color.blue);
        blue.setBackground(Color.white);
        blue.setBorder(line);
        blue.setFont(TNR);
        blue.setToolTipText("This is Blue");
        add(blue);

        // add set properties and add panel to JFrame
        cyan.setOpaque(true);
        cyan.setForeground(Color.cyan);
        cyan.setBackground(Color.white);
        cyan.setBorder(line);
        cyan.setFont(TNR);
        cyan.setToolTipText("This is Cyan");
        add(cyan);

        // add set properties and add panel to JFrame
        green.setOpaque(true);
        green.setForeground(Color.green);
        green.setBackground(Color.white);
        green.setBorder(line);
        green.setFont(TNR);
        green.setToolTipText("This is Green");
        add(green);

        // add set properties and add panel to JFrame
        magenta.setOpaque(true);
        magenta.setForeground(Color.magenta);
        magenta.setBackground(Color.white);
        magenta.setBorder(line);
        magenta.setFont(TNR);
        magenta.setToolTipText("This is Magenta");
        add(magenta);

        // add set properties and add panel to JFrame
        orange.setOpaque(true);
        orange.setForeground(Color.orange);
        orange.setBackground(Color.white);
        orange.setBorder(line);
        orange.setFont(TNR);
        orange.setToolTipText("This is Orange");
        add(orange);

        num.setOpaque(true);
        num.setForeground(Color.orange);
        num.setBackground(Color.white);
        num.setBorder(line);
        num.setFont(TNR);
        num.setToolTipText("This is Orange");
        add(num);*/


        add(p1,"East");
        add(p2,"North");
        add(p3,"West");



               p1.addPropertyChangeListener(new PropertyChangeListener() {
                   @Override
                   public void propertyChange(PropertyChangeEvent evt) {

                        int k=0;
                        int start=0;
                        for( k=0;k<teremLength;k++) {

                            String getTerem = terem[k].getText();


                            String[] arrayVeg = db.vegArray(getTerem, datum);
                            String[] arrayKezd = db.kezdArray(getTerem, datum);

                            if(arrayVeg.length<=0){
                                continue;
                            }

                            System.out.println("!!!! " + getTerem);

                            LocalTime [] VegtimeArray = new LocalTime[arrayVeg.length];
                            LocalTime [] KezdTimeArray = new LocalTime[arrayKezd.length];

                            for(int x=0; x<arrayVeg.length;x++) {

                                VegtimeArray[x]  = LocalTime.parse(arrayVeg[x]);
                                KezdTimeArray[x] = LocalTime.parse(arrayKezd[x]);
                            }

                            switch (k){
                                case 0: start=0;
                                break;
                                case 1: start=32;
                                break;
                                case 2: start=64;
                                break;
                                case 3: start=96;
                                break;
                                case 4: start=128;
                                break;
                                case 5: start=160;
                                break;
                                case 6: start=192;
                                break;
                                case 7: start=224;
                                break;
                                case 8: start=256;
                                break;
                                case 9: start=288;
                                break;
                                case 10: start=320;
                                break;
                                case 11: start=352;
                                break;
                                case 12: start=384;
                                break;
                                case 13: start=416;
                                break;
                                case 14: start=448;
                                break;
                                case 15: start=480;
                                break;
                                case 16: start=512;
                                break;
                                case 17: start=544;
                                break;
                                case 18: start=576;
                                break;
                                case 19: start=608;
                                break;
                            }


                            color(start, getTerem, KezdTimeArray, VegtimeArray);

                        }
                    }
                });


            }


       /* terem[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int startAt=32;
                String getTerem = terem[0].getText();


                String [] arrayVeg = db.vegArray(getTerem,datum);
                String [] arrayKezd = db.kezdArray(getTerem,datum);

                System.out.println("!!!! "+getTerem);

                LocalTime[] VegtimeArray = new LocalTime[arrayVeg.length];
                LocalTime [] KezdTimeArray = new LocalTime[arrayKezd.length];


                color(startAt,getTerem,KezdTimeArray,VegtimeArray);


            }
        });*/






public void color(int startAt,String teremString, LocalTime[]start,LocalTime[]end){

    LocalTime hourStratTime = null;
    LocalTime hourEndTime=null;
    int [] hourStart=new int[32];
    int [] hourEnd = new int[32];

    int minFromloop = 0;

       LocalTime [] startingTime ={LocalTime.parse("08:00"),LocalTime.parse("08:15"),LocalTime.parse("08:30"),LocalTime.parse("08:45"),
                                   LocalTime.parse("09:00"),LocalTime.parse("09:15"),LocalTime.parse("09:30"),LocalTime.parse("09:45"),
                                   LocalTime.parse("10:00"),LocalTime.parse("10:15"),LocalTime.parse("10:30"),LocalTime.parse("10:45"),
                                   LocalTime.parse("11:00"),LocalTime.parse("11:15"),LocalTime.parse("11:30"),LocalTime.parse("11:45"),
                                   LocalTime.parse("12:00"),LocalTime.parse("12:15"),LocalTime.parse("12:30"),LocalTime.parse("12:45"),
                                   LocalTime.parse("13:00"),LocalTime.parse("13:15"),LocalTime.parse("13:30"),LocalTime.parse("13:45"),
                                   LocalTime.parse("14:00"),LocalTime.parse("14:15"),LocalTime.parse("14:30"),LocalTime.parse("14:45"),
                                   LocalTime.parse("15:00"),LocalTime.parse("15:15"),LocalTime.parse("15:30"),LocalTime.parse("15:45"),
                                   LocalTime.parse("16:00")

       };


       int [] min = new int[33];
       String [] minString = new String[min.length];
       min[0] = 15;
       minString[0]= String.valueOf(min[0]);

       for (int i= 1; i<=32;i++){
           min[i]= (min[i-1]+15);
          // System.out.println(min[i]);
           minString[i]=String.valueOf(min[i]);
           //System.out.println(minString[i]);
       }



     /* LocalTime [] s = {LocalTime.parse("08:00"), LocalTime.parse("12:15")};
       LocalTime [] e = {LocalTime.parse("08:15"),LocalTime.parse("14:30")};*/

       LocalTime [] s = start;
       LocalTime [] e = end;

       /*System.out.println(s.until(e,ChronoUnit.MINUTES));

       String x = String.valueOf(s.until(e, ChronoUnit.MINUTES));*/




       for(int i=0; i<startingTime.length;i++){

            for(int k=0;k<s.length;k++){
                if(s[k].equals(startingTime[i])) {
                    hourStratTime = startingTime[i];
                    hourStart[k]=i;

                }
                if (e[k].equals(startingTime[i])) {
                    hourEndTime = startingTime[i];
                    hourEnd[k] = i;

                }

            }


       }



  /*  for(int i=0; i<startingTime.length;i++) {
        for(int k=0;k<s.length;k++) {
            if (e[k].equals(startingTime[i])) {
                hourEndTime = startingTime[i];
                hourEnd = i;

            }
        }


    }*/
  /*     for(int i=0;i<=32;i++) {


           if (x.equals(minString[i])) {

             
               minFromloop=i;

           }

       }*/

    for(int h = 0; h<hourStart.length;h++) {
        for (int i = hourStart[h]+startAt; i <= hourEnd[h]+startAt - 1; i++) {

            Button[i].setForeground(Color.RED);
            Button[i].setBackground(Color.RED);
            Button[i].setToolTipText(hourStratTime + " - " + hourEndTime);
            Button[i].setText("fog.. .");

        }
    }
}



}
