import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

public class Tablazat extends JFrame {
    private JTable jt;
    private JScrollPane sp;
    private JPopupMenu jpm;
    private JMenuItem excel;

    Tablazat(String varos, String terem, String datum) {
        super("Terem foglaltság        Város:"+varos+"     Terem Szám:"+terem+"    Dátum:"+ datum);

      //  String[] head = {"Id","Terem szám","Tanács szám","Ügyszak","Dátum","Kezdés","Befejezés"};

        Vector <String> head = new Vector<>();
        head.add("ID");
        head.add("Terem szám");
        head.add("Tanacs szám");
        head.add("Ügyszak");
        head.add("Dátum");
        head.add("Kezdés");
        head.add("Befejezés");

        Db db = new Db();

        jt = new JTable(db.teremVector(terem,datum),head);
        jt.setBounds(30, 40, 400, 300);
        sp = new JScrollPane(this.jt);
        add(this.sp);
        jpm = new JPopupMenu("Menü");
        excel = new JMenuItem("Mentés excel-be");
        jpm.add(excel);

        jt.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    jpm.show(e.getComponent(), e.getX(), e.getY());


                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        excel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filePath = db.fileChooser();
                if (db.fileNameCheck(filePath) == false) {
                    db.exportXls(jt, filePath);
                }
            }
        });

    }



}
